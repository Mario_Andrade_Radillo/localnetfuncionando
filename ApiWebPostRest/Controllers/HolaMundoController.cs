﻿using ApiWebPostRest.Models;
using System.Web.Mvc;

namespace ApiWebPostRest.Controllers
{
    public class HolaMundoController : Controller
    {
        // GET: HolaMundo
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult HolaMundo()
        {
            return Json(new {saludo="Hola Mundo" },JsonRequestBehavior.AllowGet);
        }

        //decimos que es un post y que retorna un json del tipo result
        [HttpPost]
        public JsonResult SayHello(Saludo saludo)
        {
            //es un post que recibe un objeto en formato json visual haer el parse por si solo
            saludo.SayName += " Hola bienvenido";
            //retornamos un json permitiendo las peticiones
            return Json(saludo, JsonRequestBehavior.AllowGet);
        }


    }
}