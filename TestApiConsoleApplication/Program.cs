﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using TestApiConsoleApplication.Models;

namespace TestApiConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            //Program.ApiHolaMundoGetRestTest();
            //Program.ApiSayHelloPostRestTest();
            Program.SoapCallHolaMundoTest();
            //Program.TibcoSiCallRestTest();
            Console.ReadKey();

        }

        public static void ApiHolaMundoGetRestTest()
        {
            try
            {
                HttpWebRequest Invoque = (HttpWebRequest)HttpWebRequest.Create("http://localhost:3181/HolaMundo/HolaMundo");
                Invoque.Method = "GET";
                Invoque.Accept = "application/json";
                Invoque.ContentType = "application/json";

                WebResponse response = Invoque.GetResponse();
                Stream data = response.GetResponseStream();

                StreamReader reader = new StreamReader(data);

                Console.WriteLine(reader.ReadToEnd());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error en test hola mundo: " + ex.Message);
            }
        }

        public static void ApiSayHelloPostRestTest()
        {
            try
            {
                HttpWebRequest Invoque = (HttpWebRequest)HttpWebRequest.Create("http://localhost:3181/HolaMundo/SayHello");
                Invoque.Method = "POST";
                Invoque.Accept = "application/json";
                Invoque.ContentType = "application/json";

                Respuesta req = new Respuesta();
                req.SayName = "Mario";

                //serializamos el objeto a un json string
                String dataToSend = Newtonsoft.Json.JsonConvert.SerializeObject(req);

                //lo pasamos a la codifiación que queremos para que respete los acentos 
                UTF8Encoding encode = new UTF8Encoding();
                //lo transformamos en bytes para poder mandarlo
                Byte[] bytes = encode.GetBytes(dataToSend);

                //escribimos los bytes
                //obtenemos el stream del request
                Stream reqStream = Invoque.GetRequestStream();
                //escribimos los bytes del 0 hasta el último
                reqStream.Write(bytes,0,bytes.Length);
                reqStream.Close();

                //SE OBTIENE LA RESPUESTA
                //obtenemos primero el webresponse
                WebResponse response = Invoque.GetResponse();
                //obtenemos el stream del response
                Stream data = response.GetResponseStream();
                //transformamos el stream a un stream reader para convertirlo en string
                StreamReader reader = new StreamReader(data);

                Console.WriteLine(reader.ReadToEnd());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error en test hola mundo: " + ex.Message);
            }
        }

        //Soap call
        public static void SoapCallHolaMundoTest()
        {
            try
            {
                HttpWebRequest Invoque = (HttpWebRequest)HttpWebRequest.Create("http://localhost:4107/Services/HelloWordSoapService.svc/");
                Invoque.Method = "POST";
                Invoque.Accept = "text/xml";
                //comtent type del servicio en este caso soap entonces xml
                Invoque.ContentType = "text/xml;charset=\"utf-8\"";
                //en el header agregamos el soapaction osea el método que utilizamos
                Invoque.Headers.Add("SOAPAction", "http://tempuri.org/IHelloWordSoapService/HolaMundoSoap"); ;
                //envolvemos la petición en un string en este caso no recbie parametros 
                string payload = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:tem=""http://tempuri.org/"">
                               <soapenv:Header/>
                                    <soapenv:Body>
                                    <tem:HolaMundoSoap/>
                                    </soapenv:Body>
                            </soapenv:Envelope>";

                //convertimos la petición a un arreglo de bytes 
                byte[] byteArray = Encoding.UTF8.GetBytes(payload);
                //tamaño del contenido 
                Invoque.ContentLength = byteArray.Length;
                //lo escribimos del 0 al último obteniendo el request stream
                Stream requestStream = Invoque.GetRequestStream();
                requestStream.Write(byteArray, 0, byteArray.Length);
                requestStream.Close();//cerramos el stream
                StreamReader reader=null;

                HttpWebResponse response = null;//para el response
                try
                {
                    //invocamos el response con el getresponse()
                    response = (HttpWebResponse)Invoque.GetResponse();
                    //obtenemos el stream de el response con el getresponsestream
                    Stream data = response.GetResponseStream();
                    //lo transformamos a un streamreader(flujo de lectura)
                    reader = new StreamReader(data);

                }
                catch (WebException ex)
                {
                    response = (HttpWebResponse)ex.Response;
                }


                Console.WriteLine(string.Format("HTTP/{0} {1} {2}\n", response.ProtocolVersion, (int)response.StatusCode, response.StatusDescription));
                Console.WriteLine(string.Format("{0}", reader.ReadToEnd()));//leemos hasta la última linea

                string Cad = @"<s:Envelope xmlns:s=""http://schemas.xmlsoap.org/soap/envelope/"">
<s:Body><HolaMundoSoapResponse xmlns=""http://tempuri.org/""><HolaMundoSoapResult>Hola Mundo</HolaMundoSoapResult></HolaMundoSoapResponse></s:Body></s:Envelope>";

                XDocument doc = XDocument.Parse(Cad);
                XNamespace ns = "http://tempuri.org/";
                IEnumerable<XElement> responses = doc.Descendants(ns + "HolaMundoSoapResponse");
                foreach (XElement respons in responses)
                {
                   Console.WriteLine( (string)respons.Element(ns + "HolaMundoSoapResult"));
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine("Error en test hola mundo: " + ex.Message);
            }
        }

        //tibco 6 call
        public static void TibcoSiCallRestTest()
        {
            try
            {
                HttpWebRequest Invoque = (HttpWebRequest)HttpWebRequest.Create("http://DESKTOP-UCDS8T8:9191/restservicefirst");
                Invoque.Method = "GET";
                Invoque.ContentType = "application/json";

                WebResponse response = Invoque.GetResponse();
                Stream data = response.GetResponseStream();

                StreamReader reader = new StreamReader(data);

                //Console.WriteLine(reader.ReadToEnd());

                Hello hell = new Hello();

                String aux = reader.ReadToEnd();
                hell =Newtonsoft.Json.JsonConvert.DeserializeObject<Hello>(aux);
                Console.WriteLine(hell.Saludo);
                //JsonConvert.DeserializeObject<PagedList<User>>(content, new UserPagedListConverter());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error en test TibcoSiCallRestTest: " + ex.Message);
            }
        }



    }
}
